# Fortune Cookie REST API 🥠
A fortune telling REST API web server written in Java 8 with Spring Boot and 
Postgres, secured by JWT authorization. Includes initialization scripts with 
selected fortunes from Lidl Fortune Cookies 💓


Implemented methods (so far...):

* POST ```/login``` - authorization service. Requires two JSON strings: "user" and "pass" 
* GET ```/fortune``` - get all fortunes
* GET ```/fortune/random``` - get RANDOM fortune
* GET ```/fortune/{id}``` - get specific fortune
* POST ```/fortune/{id}``` - add new fortune
* PUT ```/fortune/{id}``` - update specific fortune
* DELETE ```/fortune/{id}``` - delete specific fortune

