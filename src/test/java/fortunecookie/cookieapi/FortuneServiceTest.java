package fortunecookie.cookieapi;

import fortunecookie.cookieapi.models.Fortune;
import fortunecookie.cookieapi.models.FortuneDto;
import fortunecookie.cookieapi.repositories.FortuneRepository;
import fortunecookie.cookieapi.services.FortuneService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityNotFoundException;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Rollback
public class FortuneServiceTest {

    @Autowired
    private Environment env;

    @Autowired
    private FortuneService fortuneService;

    @Autowired
    private FortuneRepository fortuneRepository;

    @Before
    public void setUp() {
        for (int i = 1; i <= 3; i++) {
            String csText = "CS text ".concat(String.valueOf(i));
            String enText = "En text ".concat(String.valueOf(i));
            fortuneRepository.save(new Fortune(csText, enText));
        }
    }

    @Test
    public void testGetAllFortunes() throws Exception {
        assertEquals(3, fortuneService.getAll().size());
    }

    @Test
    public void testGetOne() {
        assertEquals("CS text 1", fortuneService.getOne(1).getFortune_cs());
    }

    @Test
    public void testPostOne() {
        FortuneDto newFortuneDto = new FortuneDto();
        newFortuneDto.setFortune_en("New Fortune En String");
        newFortuneDto.setFortune_cs("New Fortune Cs String");
        fortuneService.postOne(newFortuneDto);

        assertTrue(fortuneService.getAll().stream().anyMatch(
                fortuneDto -> fortuneDto.getFortune_cs().equals(newFortuneDto.getFortune_cs())));
    }

    @Test
    public void  testGetRandomOne() {
        Set<FortuneDto> result = new HashSet<>();
        for (int i = 0; i < 10; i++) {
            result.add(fortuneService.getRandomFortune());
        }

        assertNotEquals(1, result.size());
    }

    @Test(expected = EntityNotFoundException.class)
    @Rollback
    public void testDeleteOne() {
        fortuneService.deleteOne(3);
        fortuneService.getOne(3);
    }

    @Test
    @Rollback
    public void putOne() {
        FortuneDto oldFortune = fortuneService.getOne(2);

        FortuneDto updatedFortune = new FortuneDto();
        updatedFortune.setFortune_cs("New Cs string");

        fortuneService.putOne(2, updatedFortune);

        assertNotEquals(oldFortune.getFortune_cs(), fortuneService.getOne(2).getFortune_cs());
        assertEquals(updatedFortune.getFortune_cs(), fortuneService.getOne(2).getFortune_cs());
    }
}
