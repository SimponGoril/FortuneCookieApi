INSERT INTO FORTUNE (id, fortune_cs, fortune_en, date_created) VALUES (1, 'Nová vůně vás uvede do snění.', 'A new scent will make you dream.', {ts '2018-09-9 18:47:52.69'});
INSERT INTO FORTUNE (id, fortune_cs, fortune_en, date_created) VALUES (2, 'Budete mít spoustu štěstí a obtíže zdoláte.', 'You will be very lucky and overcome all difficulties.', {ts '2018-09-9 18:47:52.69'});
INSERT INTO FORTUNE (id, fortune_cs, fortune_en, date_created) VALUES (3, 'Toužíte-li po něčem, řekněte to.', 'You should talk about your desires.', {ts '2018-09-9 18:47:52.69'});
INSERT INTO FORTUNE (id, fortune_cs, fortune_en, date_created) VALUES (4, 'Po velké námaze se dostaví úspěch.', 'After this great commitment success will be sure.', {ts '2018-09-9 18:47:52.69'});
INSERT INTO FORTUNE (id, fortune_cs, fortune_en, date_created) VALUES (5, 'Dnes se vaše štěstí úplně změnilo.', 'Today your luck has changed completely.', {ts '2018-09-9 18:47:52.69'});
