$(document).ready(function() {

    // connect to rest api and retrieve a fortune
    var authorization = "Bearer ";
    $.ajax({
        type : 'POST',
        url : 'http://localhost:8080/login',
        data : {'user' : 'admin', 'pass' : 'admin'},
        dataType : 'text',
        encode : true,
        success: function (response, status, xhr) {
            authorization = authorization.concat(response);
            },
        error: function (xhr, status, error) {
            console.log("Unable to retrieve authorization token with error: " + error);
            }
        });

    // handler for cookie click
    $(".cookie").click(function() {
        $.ajax({
            type : 'GET',
            url : 'http://localhost:8080/fortune/random',
            headers: {'Authorization' : authorization},
            dataType : 'JSON',
            encode : true,
            success: function (response, status, xhr) {
                $("div[class*='cookie']").fadeOut("slow", function() {
                    $(".fortune-cs").text(response.fortune_cs).prepend("<i class=\"em em-flag-cz\"></i>&nbsp");
                    $(".fortune-en").text(response.fortune_en).prepend("<i class=\"em em-flag-um\"></i>&nbsp");
                    $(".fortune-text-wrapper").fadeIn("slow", function() {
                        $(".fortune-button-wrapper").fadeIn("slow", function() {});
                    });
                });
            },
            error: function (xhr, status, error) {
                console.log("Api error: " + error);
            }
        });
    });

    // reload button logic
    $("#reload-button").click(function() {
        $(".fortune-button-wrapper").fadeOut(300, function() {
            $(".fortune-text-wrapper").fadeOut(700, function() {
                $("div[class*='cookie']").fadeIn("slow", function() {});
            });
        });
    });
});