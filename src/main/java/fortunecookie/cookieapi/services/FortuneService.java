package fortunecookie.cookieapi.services;

import fortunecookie.cookieapi.models.Fortune;
import fortunecookie.cookieapi.models.FortuneDto;
import fortunecookie.cookieapi.repositories.FortuneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@Transactional
public class FortuneService {

    @Autowired
    private FortuneRepository fortuneRepository;

    private static Random random = new Random();

    /**
     * Returns random fortune from associated datasource in form of simplified dto or throw ResourceNotFoundException
     * in case of empty database
     * @return random fortune dt object
     */
    public FortuneDto getRandomFortune() {
        int availableFortuneCount = (int)fortuneRepository.count();
        if (availableFortuneCount == 0) {
            throw new ResourceNotFoundException("No fortunes found in datasource");
        }

        Fortune randomFortune = fortuneRepository.getOne((random.nextInt(availableFortuneCount) + 1));
        return new FortuneDto(randomFortune);
    }

    /**
     * Returns all fortunes as a list of dto
     * @return all available fortunes
     */
    public List<FortuneDto> getAll() {
        List<Fortune> fortunes = fortuneRepository.findAll();
        return fortunes.stream().map(fortune -> new FortuneDto(fortune))
                                .collect(Collectors.toList());
    }

    /**
     * Returns specified page of fortunes
     * @param page starting page of fortunes
     * @param size limit of fortunes to be retrieved
     * @return list of fortunes from specified interval
     */
    public List<FortuneDto> getSome(int page, int size) {
        Page<Fortune> fortunes = fortuneRepository.findAll(PageRequest.of(page, size));
        return fortunes.getContent().stream().map(fortune -> new FortuneDto(fortune))
                                .collect(Collectors.toList());
    }

    /**
     * Return one fortune with specified id
     * @param id of desired fortune
     * @return fortune dto object
     */
    public FortuneDto getOne(int id) {
        return new FortuneDto(fortuneRepository.getOne(id));
    }

    /**
     * Update fortune in datasource or throw ResourceNotFoundException
     * @param id id of fortune to be updated
     * @param updatedFortune updated values
     * @return updated fortune as dto
     */
    public FortuneDto putOne(int id, FortuneDto updatedFortune) {
        return fortuneRepository.findById(id).map(fortune -> {fortune.setFortune_cs(updatedFortune.getFortune_cs());
                                                              fortune.setFortune_en(updatedFortune.getFortune_en());
                                                              return new FortuneDto(fortuneRepository.save(fortune));})
                                             .orElseThrow(() -> new ResourceNotFoundException());
    }

    /**
     * Saves new fortune to datasource
     * @param newFortuneDto new fortune to be saved in database
     * @return new fortune as dto
     */
    public FortuneDto postOne(FortuneDto newFortuneDto) {
        Fortune newFortune = new Fortune(newFortuneDto.getFortune_cs(), newFortuneDto.getFortune_en());

        return new FortuneDto(fortuneRepository.save(newFortune));
    }

    /**
     * Delete fortune with specific id or throw ResourceNotFoundException. Return boolean true if successful
     * @param id id of fortune to be deleted
     * @return boolean true if fortune was deleted
     */
    public boolean deleteOne(int id) {
        return fortuneRepository.findById(id).map(fortune -> {fortuneRepository.delete(fortune);
                                                       return true;})
                                      .orElseThrow(() -> new ResourceNotFoundException());
    }
}
