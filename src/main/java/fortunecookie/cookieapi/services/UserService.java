package fortunecookie.cookieapi.services;


import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserService  {

    @Autowired
    private Environment env;

    private static BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    private Logger logger = LogManager.getLogger(UserService.class);

    /**
     * Very basic authorization method that compare user nad password from login POST request
     * with environment variables and generates JWT token for API calls
     */
    public String login(String user, String pass) {
            final String adminUsername = env.getProperty("spring.security.user.name");
            final String adminPassword = env.getProperty("spring.security.user.password");

            if (!user.equals(adminUsername) || !encoder.matches(pass, adminPassword)) {
                logger.debug("Wrong credentials for user {}", adminUsername);
                throw new AuthenticationCredentialsNotFoundException("Invalid credentials");
            }

            // if the password is ok, generate token for further requests
            String jwtToken = Jwts.builder().setSubject(user).claim("roles", "ADMIN").setIssuedAt(new Date())
                    .signWith(SignatureAlgorithm.HS256, "secretkey").compact();
            logger.debug("User {} received authorization JWT", adminUsername);
            return jwtToken;
        }

    }

