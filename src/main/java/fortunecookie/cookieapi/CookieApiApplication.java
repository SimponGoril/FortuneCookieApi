package fortunecookie.cookieapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import javax.servlet.Filter;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.filter.ShallowEtagHeaderFilter;

@SpringBootApplication(scanBasePackages = {  "fortunecookie.cookieapi" })
@EnableJpaAuditing
public class CookieApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CookieApiApplication.class, args);
    }

    /**
     * Add shallow ETag cache support
     */
    @Bean
    public Filter filter(){
        ShallowEtagHeaderFilter filter=new ShallowEtagHeaderFilter();
        return filter;
    }

}
