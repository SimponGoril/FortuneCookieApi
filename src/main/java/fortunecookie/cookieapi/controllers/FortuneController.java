package fortunecookie.cookieapi.controllers;

import fortunecookie.cookieapi.models.FortuneDto;
import fortunecookie.cookieapi.services.FortuneService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Rest controller servicing requests for fortunes
 */
@RestController
@RequestMapping("/fortune")
public class FortuneController {

    @Autowired
    private FortuneService fortuneService;

    private Logger logger = LogManager.getLogger(LoginController.class);

    /**
     * Returns all available fortunes in specified interval
     * @return list of fortune dtos
     */
    @GetMapping(value = "/", produces = "application/json")
    public List<FortuneDto> getSome(@RequestParam(value = "page", defaultValue = "0") int page,
                                    @RequestParam(value = "limit", defaultValue = "10") int limit) {
        logger.debug("Fortune API: retrieving fortune from page {} with limit {}", page, limit);
        return fortuneService.getSome(page, limit);
    }

    /**
     * Returns desired fortune with specified id
     * @param id of fortune
     * @return fortune dto as json
     */
    @GetMapping(value = "/{id}", produces = "application/json")
    public FortuneDto getOne(@PathVariable int id) {
        logger.debug("Fortune API: retrieving fortune {}", id);
        return fortuneService.getOne(id);
    }

    /**
     * Updates fortune with specified id
     * @param id of fortune
     * @param updatedFortune updated fortune JSON
     * @return updated fortune dto as json
     */
    @PutMapping(value = "/{id}", produces = "application/json")
    public FortuneDto putOne(@PathVariable int id, @RequestBody FortuneDto updatedFortune) {
        logger.debug("Fortune API: updating fortune {}", id);
        return fortuneService.putOne(id, updatedFortune);
    }

    /**
     * Post new fortune to datasource
     * @param newFortune
     * @return new fortune dto as json
     */
    @PostMapping(value = "/", consumes = "application/json")
    public FortuneDto postOne(@RequestBody FortuneDto newFortune) {
        logger.debug("Fortune API: adding fortune");
        return fortuneService.postOne(newFortune);
    }

    /**
     * Delete fortune by id
     * @param id of fortune to be deleted
     */
    @DeleteMapping(value = "/{id}")
    public void deleteOne(@PathVariable int id) {
        logger.debug("Fortune API: deleting fortune {}", id);
        fortuneService.deleteOne(id);

    }

    /**
     * Returns random fortune DTO as JSON
     * @return random fortune dto as json
     */
    @GetMapping(value = "/random", produces = "application/json")
    public FortuneDto getOneRandom() {
        logger.debug("Fortune API: retrieving random fortune");
        return fortuneService.getRandomFortune();
    }
}
