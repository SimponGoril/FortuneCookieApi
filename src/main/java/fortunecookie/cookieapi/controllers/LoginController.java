package fortunecookie.cookieapi.controllers;

import fortunecookie.cookieapi.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest controller servicing login and authorization
 */
@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private UserService userService;

    private Logger logger = LogManager.getLogger(LoginController.class);

    /**
     * Main login method. Return JWT token authorizing api calls
     * @param userName env user
     * @param userPass env password
     * @return
     */
    @PostMapping
    public String login(@RequestParam(value = "user") String userName,
                        @RequestParam(value = "pass") String userPass) {
        logger.debug("User {} is attempting to login", userName);
        return userService.login(userName, userPass);
    }
}
