package fortunecookie.cookieapi.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Main controller that routes all requests to index page
 */
@Controller
public class MainController {

    private final static String INDEX_PAGE_NAME = "index";

    @RequestMapping("/*")
    public String serveWelcomePage() {
        return INDEX_PAGE_NAME;
    }
}
