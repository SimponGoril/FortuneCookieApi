package fortunecookie.cookieapi.repositories;

import fortunecookie.cookieapi.models.Fortune;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FortuneRepository extends JpaRepository<Fortune, Integer> {
}
