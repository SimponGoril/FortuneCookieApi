package fortunecookie.cookieapi.models;

import javax.persistence.*;
import java.time.Instant;
import java.util.Date;

@Entity
@Table(name = "FORTUNE")
public class Fortune {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(columnDefinition = "fortune_cs")
    private String fortune_cs;

    @Column(columnDefinition = "fortune_en")
    private String fortune_en;

    @Column(columnDefinition = "date_created")
    private Date date_created;

    public Fortune() {}

    public Fortune(String fortune_cs, String fortune_en) {
        this.fortune_cs = fortune_cs;
        this.fortune_en = fortune_en;
        this.date_created = new Date(Instant.now().toEpochMilli());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFortune_cs() {
        return fortune_cs;
    }

    public void setFortune_cs(String fortune_cs) {
        this.fortune_cs = fortune_cs;
    }

    public String getFortune_en() {
        return fortune_en;
    }

    public void setFortune_en(String fortune_en) {
        this.fortune_en = fortune_en;
    }

    public Date getDate_created() {
        return date_created;
    }

    public void setDate_created(Date date_created) {
        this.date_created = date_created;
    }
}
