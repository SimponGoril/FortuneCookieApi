package fortunecookie.cookieapi.models;

public class FortuneDto {

    private int id;

    private String fortune_cs;

    private String fortune_en;

    public FortuneDto() {}

    public FortuneDto(Fortune fortune) {
        this.setId(fortune.getId());
        this.setFortune_cs(fortune.getFortune_cs());
        this.setFortune_en(fortune.getFortune_en());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFortune_cs() {
        return fortune_cs;
    }

    public void setFortune_cs(String fortune_cs) {
        this.fortune_cs = fortune_cs;
    }

    public String getFortune_en() {
        return fortune_en;
    }

    public void setFortune_en(String fortune_en) {
        this.fortune_en = fortune_en;
    }
}
